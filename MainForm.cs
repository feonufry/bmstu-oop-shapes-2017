﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Hecsit.Oop.Intoduction.Shapes;
using Rectangle = Hecsit.Oop.Intoduction.Shapes.Rectangle;

namespace Hecsit.Oop.Intoduction
{
	public partial class MainForm : Form
	{
		private IShape _captured = null;
		private Point _mouse;

		private readonly List<IShape> _shapes = new List<IShape>
		{
			new ComplexShape(new Point(100, 100),
				new Rectangle(new Point(0, 0), new Size(200, 25), Color.LightSteelBlue),
				new Rectangle(new Point(10, 25), new Size(20, 75), Color.LightSteelBlue),
				new Rectangle(new Point(170, 25), new Size(20, 75), Color.LightSteelBlue)
			),
			new Circle(new Point(100, 75), 25, Color.ForestGreen),
			//new Circle(new Point(350, 100), 50, Color.Gold),
			//new Rectangle(new Point(0, 25), new Size(100, 35), Color.DarkRed),
			//new Rectangle(new Point(25, 0), new Size(50, 35), Color.DarkRed)
		};
		
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Paint(object sender, PaintEventArgs e)
		{
			foreach (var circle in _shapes)
			{
				circle.Draw(e.Graphics);
			}
		}

		private void MainForm_MouseDown(object sender, MouseEventArgs e)
		{
			_mouse.X = e.X;
			_mouse.Y = e.Y;

			for (var i = _shapes.Count - 1; i >= 0; --i)
			{
				var circle = _shapes[i];
				if (circle.Contains(new Point(e.X, e.Y)))
				{
					_captured = circle;
					break;
				}
			}
		}

		private void MainForm_MouseMove(object sender, MouseEventArgs e)
		{
			if (_captured == null) return;

			_captured.Move(e.X - _mouse.X, e.Y - _mouse.Y);
			
			_mouse.X = e.X;
			_mouse.Y = e.Y;
			Invalidate();
		}

		private void MainForm_MouseUp(object sender, MouseEventArgs e)
		{
			_captured = null;
		}
	}
}
