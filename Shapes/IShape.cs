﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
	public interface IShape
	{
		void Draw(Graphics graphics);
		bool Contains(Point point);
		void Move(int offsetX, int offsetY);
	}
}