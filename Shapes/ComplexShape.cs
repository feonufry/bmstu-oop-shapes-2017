﻿using System.Collections.Generic;
using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
	public class ComplexShape : IShape
	{
		private Point _basePoint;
		private readonly List<IShape> _children = new List<IShape>();

		public ComplexShape(Point basePoint, params IShape[] shapes)
		{
			_basePoint = basePoint;
			_children.AddRange(shapes);
		}

		public void Draw(Graphics graphics)
		{
			graphics.TranslateTransform(_basePoint.X, _basePoint.Y);
			foreach (var child in _children)
			{
				child.Draw(graphics);
			}
			graphics.TranslateTransform(-_basePoint.X, -_basePoint.Y);
		}

		public bool Contains(Point point)
		{
			var local = new Point(point.X - _basePoint.X, point.Y - _basePoint.Y);
			foreach (var child in _children)
			{
				if (child.Contains(local))
				{
					return true;
				}
			}
			return false;
		}

		public void Move(int offsetX, int offsetY)
		{
			_basePoint = new Point(_basePoint.X + offsetX, _basePoint.Y + offsetY);
		}
	}
}