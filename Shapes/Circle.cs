﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
	public class Circle : IShape
	{
		private Point _center;
		private readonly int _radius;
		private readonly Color _color;

		public Circle(Point center, int radius, Color color)
		{
			_center = center;
			_radius = radius;
			_color = color;
		}

		public void Draw(Graphics graphics)
		{
			graphics.FillEllipse(
				new SolidBrush(_color), 
				_center.X - _radius, _center.Y - _radius, 
				2*_radius, 2*_radius);
		}

		public bool Contains(Point point)
		{
			return (point.X - _center.X)*(point.X - _center.X)
				+ (point.Y - _center.Y)*(point.Y - _center.Y) <= _radius*_radius;
		}

		public void Move(int offsetX, int offsetY)
		{
			_center = new Point(_center.X + offsetX, _center.Y + offsetY);
		}
	}
}