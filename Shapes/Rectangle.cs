﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
	public class Rectangle : IShape
	{
		private Point _leftTop;
		private readonly Size _size;
		private readonly Color _color;

		public Rectangle(Point leftTop, Size size, Color color)
		{
			_color = color;
			_leftTop = leftTop;
			_size = size;
		}

		public Rectangle(Point leftTop, Point rightBottom, Color color)
			: this(leftTop, new Size(rightBottom.X - leftTop.X, rightBottom.Y - leftTop.Y), color)
		{}

		public void Draw(Graphics graphics)
		{
			graphics.FillRectangle(
				new SolidBrush(_color), 
				_leftTop.X, _leftTop.Y, 
				_size.Width, _size.Height);
		}

		public bool Contains(Point point)
		{
			return point.X >= _leftTop.X && point.X <= _leftTop.X + _size.Width
			       && point.Y >= _leftTop.Y && point.Y <= _leftTop.Y + _size.Height;
		}

		public void Move(int offsetX, int offsetY)
		{
			_leftTop = new Point(_leftTop.X + offsetX, _leftTop.Y + offsetY);
		}
	}
}